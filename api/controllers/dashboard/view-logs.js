module.exports = {


  friendlyName: 'View logs page',


  description: 'Display the "Logs" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/dashboard/logs',
      description: 'Display the logs page for authenticated users.'
    },

  },


  fn: async function () {

    return {};

  }


};
