/**
 * FileActionLog.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    file: {
      model: 'userfile'
    },
    actionType: {
      type: 'string', // create, share and download
    },
    // Add a reference to User
    doneBy: {
      model: 'user'
    }

  },

};

