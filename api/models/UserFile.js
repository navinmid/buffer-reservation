/**
 * UserFile.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    name: {
      type: 'String'
    },
    file: {
      type: 'String'
    },
    // Add a reference to User
    owner: {
      model: 'user'
    }

  },

};

