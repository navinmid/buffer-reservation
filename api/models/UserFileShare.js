/**
 * UserFileShare.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    
    file: {
      model: 'userfile'
    },
    sharedWith: {
      model: 'user'
    },
    // Add a reference to User
    sharedBy: {
      model: 'user'
    }

  },

};

