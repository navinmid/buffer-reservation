Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(value).format('MM/DD/YYYY hh:mm')
  }
});
parasails.registerPage('welcome', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    inputBytes: 150,
    bufferSize: 100,
    receivingBytes: 20,
    receivedSoFar: 0,
    bufferCurrentValue: 0,
    logs: []
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function() {
    //…
  },

  //  ╦  ╦╦╦═╗╔╦╗╦ ╦╔═╗╦    ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ╚╗╔╝║╠╦╝ ║ ║ ║╠═╣║    ╠═╝╠═╣║ ╦║╣ ╚═╗
  //   ╚╝ ╩╩╚═ ╩ ╚═╝╩ ╩╩═╝  ╩  ╩ ╩╚═╝╚═╝╚═╝
  // Configure deep-linking (aka client-side routing)
  virtualPagesRegExp: /^\/welcome\/?([^\/]+)?\/?/,
  afterNavigate: async function(virtualPageSlug){
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    send: async function() {

      var _this = this,
        receivingBytes = parseInt(this.receivingBytes);

      if(receivingBytes >= _this.bufferSize || _this.inputBytes <= _this.bufferSize) {
        alert("Invalid range");
        return;
      }

      _this.bufferCurrentValue = _this.bufferSize;
      _this.receivedSoFar = receivingBytes;
      _this.bufferCurrentValue -= _this.receivedSoFar;
      var timeout = setInterval(function(){
        if(_this.receivedSoFar < _this.inputBytes) {
          _this.receivedSoFar += receivingBytes;
          if(_this.receivedSoFar > _this.inputBytes) {
            clearInterval(timeout);
            _this.receivedSoFar = _this.inputBytes;
          }
          if((_this.bufferSize - receivingBytes) > (_this.inputBytes - _this.receivedSoFar)) {
            _this.bufferCurrentValue = _this.inputBytes - _this.receivedSoFar;
          }
          if(_this.bufferCurrentValue <= 0) {
            _this.bufferCurrentValue = 0;
          }
        }
      }, 1000);
    }
  }
});
